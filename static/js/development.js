var simplemde = new SimpleMDE({
    element: $("#issue_content")[0],
    hideIcons: ["side-by-side", "fullscreen"]
});